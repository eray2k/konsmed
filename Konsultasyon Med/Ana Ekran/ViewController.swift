//
//  ViewController.swift
//  Konsultasyon Med
//
//  Created by Eray Beşirli on 24.03.2019.
//  Copyright © 2019 Eray Besirli. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func hastalikButtonClicked(_ sender: UIButton) {
        performSegue(withIdentifier: "ToHastalik", sender: nil)
    }
    @IBAction func receteButtonClicked(_ sender: UIButton) {
        performSegue(withIdentifier: "ToRecete", sender: nil)
    }
    
    @IBAction func dozHesaplaButtonClicked(_ sender: UIButton) {
        performSegue(withIdentifier: "ToDoz", sender: nil)
    }
}


